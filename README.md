# tiq-tests

This tool gathers threat intelligence data from feeds and runs tests on them.

## Usage: 

(in cmd line)

~~~~
python combine/combine.py --enrich --tiq-test
~~~~

This gathers feed data from URLs listed in inbound_urls.txt and saves data to folder tiq_test\data.

Then the data can be analyzed in R by issuing

~~~~
source("tiq_main.R")
~~~~


