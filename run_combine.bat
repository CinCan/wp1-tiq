REM This Windows batch file runs "combine" tool
REM to fetch feed data suitable for tiq-test analysis
REM and pushes to Gitlab

cd combine

python combine.py --enrich --tiq-test

cd ..

git pull
git add combine\tiq_test\data\*.csv.gz
git commit -m "new data in"
git push

pause